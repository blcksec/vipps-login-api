<!-- START_METADATA
---
title: Introduction
sidebar_position: 1
hide_table_of_contents: true
pagination_next: null
pagination_prev: null
---
END_METADATA -->

# Vipps Login API

<!-- START_COMMENT -->

ℹ️ Please use the new documentation:
[Vipps Technical Documentation](https://vippsas.github.io/vipps-developer-docs/).

<!-- END_COMMENT -->

For information and to order the product go to:

* [Product information in Norwegian](https://www.vipps.no/produkter-og-tjenester/bedrift/innlogging-og-identifisering/logg-inn-med-vipps/)
* [Product FAQ in Norwegian](https://vipps.no/hjelp/vipps/vipps-logg-inn)

## How it works

* [Vipps Login in browser: How It Works](./vipps-login-api-howitworks.md)
* [Vipps Login from phone number: How It Works](./vipps-login-from-phone-number-api-howitworks.md)
* [Vipps Login + Vipps Recurring: How It Works](vipps-login-recurring-howitworks.md)

## Getting started

See
[Getting Started](https://vippsas.github.io/vipps-developer-docs/docs/vipps-developers/vipps-getting-started)
for information about API keys, product activation, how to make API calls, etc.


## Developer guide

* [API Quick Start](vipps-login-api-quick-start.md): Quick Start.
* [API Guide](vipps-login-api.md): In-depth guide to the Login API.
* [API FAQ](vipps-login-api-faq.md): Questions and answers.
* [API Reference](https://vippsas.github.io/vipps-developer-docs/api/login): Login API reference documentation.

## Questions

We're always happy to help with code or other questions you might have!
Please create an [issue](https://github.com/vippsas/vipps-login-api/issues),
a [pull request](https://github.com/vippsas/vipps-login-api/pulls),
or [contact us](https://vippsas.github.io/vipps-developer-docs/docs/vipps-developers/contact).

Sign up for our [Technical newsletter for developers](https://vippsas.github.io/vipps-developer-docs/docs/vipps-developers/newsletters).
